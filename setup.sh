#!/bin/bash

# ------------------------------------------------------------------------------
# Description:
#
#   This script is used to configure the environment for GAMIL. You can source
#   it in your ~/.bashrc, or source manually in your shell.
#
# Authors:
#
#   Li Dong <dongli@lasg.iap.ac.cn> - 2013-04-11
# ------------------------------------------------------------------------------

export GAMIL_ROOT=$(cd $(dirname $BASH_ARGV) && pwd)
export GAMIL_TOOLS=$GAMIL_ROOT/tools
export PATH=$GAMIL_TOOLS:$PATH
