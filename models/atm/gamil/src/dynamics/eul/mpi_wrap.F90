! ------------------------------------------------------------------------------
! Description:
!
!   This module provides a temporal interface the new MPI module.
! 
! Authors:
!
!   Li Dong <dongli@lasg.iap.ac.cn> - 2013-04-03
! ------------------------------------------------------------------------------

module mpi_wrap

    use pmgrid
    use mpi_params
    use mpi_gamil

    implicit none

contains

    subroutine mpi_wrap_init(num_lon_, num_lat_, num_lev_)

        integer, intent(in) :: num_lon_, num_lat_, num_lev_

        character(50), parameter :: sub_name = "mpi_wrap_init"

        num_lon = num_lon_
        num_lat = num_lat_
        num_lev = num_lev_

        idx_start_lon = ibeg1
        idx_end_lon   = iend1
        idx_start_lat = jbeg0
        idx_end_lat   = jend0
        idx_start_lon_with_bnd = beglonex
        idx_end_lon_with_bnd = endlonex
        idx_start_lat_with_bnd = merge(jbeg0, beglatexdyn, is_north_pole(jbeg0))
        idx_end_lat_with_bnd = merge(jend0, endlatexdyn, is_south_pole(jend0))
        idx_start_lat_no_pole = jbeg1
        idx_end_lat_no_pole   = jend1

        num_lon_subdom = idx_end_lon-idx_start_lon+1
        num_lat_subdom = idx_end_lat-idx_start_lat+1

    end subroutine mpi_wrap_init

    function is_pole(j) result(res)
    
        integer, intent(in) :: j
        logical res
    
        if (j == 1 .or. j == num_lat) then
            res = .true.
        else
            res = .false.
        end if
    
    end function is_pole
    
    function is_south_pole(j) result(res)
    
        integer, intent(in) :: j
        logical res
    
        if (j == num_lat) then
            res = .true.
        else
            res = .false.
        end if
    
    end function is_south_pole
    
    function is_north_pole(j) result(res)
    
        integer, intent(in) :: j
        logical res
    
        if (j == 1) then
            res = .true.
        else
            res = .false.
        end if

    end function is_north_pole

end module mpi_wrap
