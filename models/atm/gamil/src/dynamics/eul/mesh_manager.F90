! ------------------------------------------------------------------------------
! Description:
!
!   This module manages the model mesh grids. Set the parameters of mesh.
! 
! Authors:
!
!   Li Dong <dongli@lasg.iap.ac.cn> - 2013-04-11
! ------------------------------------------------------------------------------

module mesh_manager

    ! ##############
    ! OLD GAMIL PART
    use shr_kind_mod, only: r8 => shr_kind_r8
    use pmgrid, only: plond, beglatexdyn, endlatexdyn, numbnd        
    use infnan
    use mpi_gamil
    use stdatm
    use dynconst
    use rgrid
    ! ##############
    use mpi_wrap
    use mesh_params
    use vertical_coordinate

    implicit none

    private

    public mesh_manager_init
    public mesh_manager_final

    public is_even_area

    logical :: is_even_area = .true.   

contains

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Initialize mesh manager.
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn> - 2013-04-11
    ! --------------------------------------------------------------------------

    subroutine mesh_manager_init()

#include <comctl.h>

        real(r8) dA0
        integer i, j, k
        real(r8) north, south
        real(r8), parameter :: pi = 4.0*atan(1.0)

        character(50), parameter :: sub_name = "mesh_manager_init"

        ! ##############
        ! OLD GAMIL PART
        allocate(w(num_lat))
        allocate(clat(num_lat))
        allocate(clon(num_lon+2,num_lat))
        allocate(latdeg(num_lat))
        allocate(londeg(num_lon+2,num_lat))
        allocate(ythu(1-numbnd:num_lat+numbnd))
        allocate(ythv(1-numbnd:num_lat+numbnd))
        allocate(wtgu(1-numbnd:num_lat+numbnd))
        allocate(wtgv(1-numbnd:num_lat+numbnd))
        allocate(sinu(beglatexdyn:endlatexdyn))
        allocate(sinv(beglatexdyn:endlatexdyn))
        allocate(oux(beglatexdyn:endlatexdyn))
        allocate(ouy(beglatexdyn:endlatexdyn))
        allocate(ovx(beglatexdyn:endlatexdyn))
        allocate(ovy(beglatexdyn:endlatexdyn))
        allocate(ff(beglatexdyn:endlatexdyn))
        allocate(cur(beglatexdyn:endlatexdyn))

        sinu = inf
        sinv = inf
        oux  = inf
        ouy  = inf
        ovx  = inf
        ovy  = inf
        ff   = inf
        cur  = inf
     
        allocate(mdj(beglatexdyn:endlatexdyn))
        allocate(mm1(ilbnd:ihbnd,beglatexdyn:endlatexdyn))
        allocate(mp1(ilbnd:ihbnd,beglatexdyn:endlatexdyn))
        allocate(mm2(ilbnd:ihbnd,beglatexdyn:endlatexdyn))
        allocate(mp2(ilbnd:ihbnd,beglatexdyn:endlatexdyn))
        allocate(mm3(ilbnd:ihbnd,beglatexdyn:endlatexdyn))
        allocate(mp3(ilbnd:ihbnd,beglatexdyn:endlatexdyn))

        mdj = bigint 
        mm1 = bigint 
        mp1 = bigint 
        mm2 = bigint 
        mp2 = bigint 
        mm3 = bigint 
        mp3 = bigint 

        allocate(dA_full(idx_start_lon:idx_end_lon,idx_start_lat:idx_end_lat))
        allocate(dA_half(idx_start_lon:idx_end_lon,idx_start_lat:idx_end_lat))
        allocate(dV_full(idx_start_lon:idx_end_lon,idx_start_lat:idx_end_lat,num_lev))
        allocate(dV_half(idx_start_lon:idx_end_lon,idx_start_lat:idx_end_lat,num_lev))

        call vertical_coordinate_init(num_lev)
        call vpar(pmtop, p00, sig, sigl, dsig)

        call span(mm1, mm2, mm3, mp1, mp2, mp3, mdj)

        call latmesh(is_even_area, num_lat, dy, ythu(1), ythv(1), wtgu(1), wtgv(1))

        w(1)    = 1-cos(0.5*ythu(2))
        w(num_lat) = w(1) 
    
        do j = 2, num_lat/2
            north = 0.5*(ythu(j-1)+ythu(j))
            south = 0.5*(ythu(j+1)+ythu(j))
            w(j)  = cos(north)-cos(south)
            w(plat+1-j) = w(j)
        end do
    
        do j = 1, num_lat
            clat(j) = ythu(j)-0.5d0*pi     
        end do
     
        do j = 1, num_lat
            latdeg(j) = clat(j)*45./atan(1._r8)
        end do
    
        dx = pi*2.0/dble(num_lon)
    
        call hpar(dx, dy, ythu(beglatexdyn), ythv(beglatexdyn), &
                  wtgu(beglatexdyn), wtgv(beglatexdyn), mdj, &
                  sinu, sinv, oux, ouy, ovx, ovy, ff, cur)

        fullgrid = .true.
        do j = 1, num_lat
            if (nlon(j) < plon) fullgrid = .false.
        end do

        do j = 1, num_lat
        do i = 1, nlon(j)
            londeg(i,j) = (i-1)*360./nlon(j)
            clon(i,j)   = (i-1)*2.0*pi/nlon(j)
        end do
        end do
        ! ##############

        dA0 = RAD**2*dx*dy
        do j = idx_start_lat_no_pole, idx_end_lat_no_pole
            dA_full(:,j) = sinu(j)/wtgu(j)*dA0
            dA_half(:,j) = sinv(j)/wtgv(j)*dA0
        end do
        ! NOTE: The Poles is from North Pole to South Pole.
        if (is_north_pole(idx_start_lat)) then
            j = idx_start_lat
            dA_full(:,j) = sinv(1)/wtgv(1)*dA0*0.25d0
            dA_half(:,j) = 4.0d0*dA_full(idx_start_lon,j)
        end if
        if (is_south_pole(idx_end_lat)) then
            j = idx_end_lat
            dA_full(:,j) = sinv(j-1)/wtgv(j-1)*dA0*0.25d0
            dA_half(:,j) = 0.0d0
        end if
        do k = 1, num_lev
            dV_full(:,:,k) = dA_full(:,:)*dsig(k)
            dV_half(:,:,k) = dA_half(:,:)*dsig(k)
        end do

    end subroutine mesh_manager_init

    ! --------------------------------------------------------------------------
    ! Description:
    !
    ! Authors:
    !
    ! --------------------------------------------------------------------------

    subroutine mesh_manager_final()

        character(50), parameter :: sub_name = "mesh_manager_final"

        deallocate(w)
        deallocate(clat)
        deallocate(clon)
        deallocate(latdeg)
        deallocate(londeg)
        deallocate(ythu)
        deallocate(ythv)
        deallocate(wtgu)
        deallocate(wtgv)
        deallocate(sinu)
        deallocate(sinv)
        deallocate(oux)
        deallocate(ouy)
        deallocate(ovx)
        deallocate(ovy)
        deallocate(ff)
        deallocate(cur)
        deallocate(mdj)
        deallocate(mm1)
        deallocate(mp1)
        deallocate(mm2)
        deallocate(mp2)
        deallocate(mm3)
        deallocate(mp3)
        deallocate(dA_full)
        deallocate(dA_half)
        deallocate(dV_full)
        deallocate(dV_half)

    end subroutine mesh_manager_final

end module mesh_manager
