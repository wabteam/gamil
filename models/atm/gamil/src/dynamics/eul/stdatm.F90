module stdatm

    use shr_kind_mod, only: r8 => shr_kind_r8
    use pmgrid, only: plevstd
    use infnan

    implicit none

    real(r8) :: tbb(plevstd) = inf
    real(r8) :: hbb(plevstd) = inf
    real(r8) :: cbb(plevstd) = inf
    real(r8) :: dcbb(plevstd) = inf
    real(r8) :: p00 = inf, t00 = inf

end module stdatm
