#include <misc.h>
#include <params.h>

subroutine cldinti

    use shr_kind_mod,   only: r8 => shr_kind_r8
    use pmgrid,         only: plev, plevp, masterproc
    use cldconst
    use console
    use string
    use vertical_coordinate

    character(*), parameter :: sub_name = "cldinti"

    !
    ! Find vertical level nearest 700 mb
    !
    k700 = 1
    do k = 1, plev-1
        if (hypm(k) < 7.0e4 .and. hypm(k+1) >= 7.0e4) then
            if (7.0e4-hypm(k) < hypm(k+1)-7.0e4) then
                k700 = k
            else
                k700 = k+1
            end if
            goto 20
        end if
    end do

    call error("Model levels bracketing 700mb are not found!", sub_name, __LINE__)

20  continue

    call notice("Model level nearest 700mb is "//trim(tr(k700))// &
                " which is "//trim(tr(hypm(k700), "F10.2"))//".", sub_name)

    return
end subroutine cldinti
