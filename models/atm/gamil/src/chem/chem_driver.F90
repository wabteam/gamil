! --------------------------------------------------------------------------
! Description:
!
!   This module is the driver for chemistry.
!
!   The moisture is excluded for the time being.
!
! Authors:
!
!   Li Dong <dongli@lasg.iap.ac.cn> - 2013-03-31
! --------------------------------------------------------------------------

module chem_driver

    ! ##############
    ! OLD GAMIL PART
    use shr_kind_mod, only: r8 => shr_kind_r8
    use time_manager, only: dt => dtdy 
    use dynconst, only: radius => RAD
    use mpi_gamil
    ! ##############
    ! GEOS-CHEM PART
    use GIGC_ENVIRONMENT_MOD
    use GIGC_INPUT_OPT_MOD
    use GIGC_STATE_CHM_MOD
    use GIGC_STATE_MET_MOD
    use LOGICAL_MOD
    use INPUT_MOD
    use TRACERID_MOD
    use TRACER_MOD
    use PRESSURE_MOD
    use COMODE_LOOP_MOD
    use TIME_MOD
    use TROPOPAUSE_MOD
    use LINOZ_MOD
    use GCKPP_COMODE_MOD
    use COMODE_MOD
    use GLOBAL_CH4_MOD
    use MEGAN_MOD
    use GRID_MOD
    use RESTART_MOD
    ! ##############
    use netcdf
    use console
    use mpi_params
    use mesh_params
    use advection_scheme

    implicit none

    private

    public chem_driver_init
    public chem_driver_run_dynamics
    public chem_driver_run_physics
    public chem_driver_run_chemistry
    public chem_driver_final

    public is_chem_on

    logical :: is_chem_on = .false.

    type(optinput) input_opt
    type(chmstate) state_chm
    type(metstate) state_met

contains

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Initialize chemistry driver.
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn> - 2013-03-31
    ! --------------------------------------------------------------------------
    
    subroutine chem_driver_init()

        integer NYMD, NYMDb, NHMS, NHMSb
        real(r8) TAU, TAUb
        real(r8), parameter :: pi = 4.0d0*atan(1.0d0)
        real(r8), parameter :: rad2deg = 180.0d0/pi

        integer i, j, k, NC

        character(*), parameter :: sub_name = "chem_driver_init"

        if (.not. is_chem_on) return

        call notice("Initialize GEOS-Chem.", sub_name)

        ! ----------------------------------------------------------------------
        input_opt%max_diag = 70
        input_opt%max_trcs = 1
        input_opt%max_memb = 1
        input_opt%max_fams = 2
        ! ----------------------------------------------------------------------
        ! calling several GEOS-Chem subroutines to do initialization
        call GIGC_ALLOCATE_ALL(is_rootproc, input_opt, NC, &
                               idx_start_lon-1, idx_start_lat, &
                               idx_end_lon-1, idx_end_lat, &
                               num_lon_subdom, num_lat_subdom, num_lev, &
                               num_lon, num_lat, num_lev)

        call READ_INPUT_FILE(is_rootproc, input_opt, NC)

        ! ############################################################################
        ! NOTE: Set GEOS-Chem grids to GAMIL grids to overwrite the default ones.
        ! XMID(num_lon,num_lat,num_lev), XEDGE, YMID, YEDGE, YMID_R, YEDGE_R, AERA_M2
        do k = 1, num_lev
        do j = idx_start_lat, idx_end_lat
        do i = idx_start_lon-1, idx_end_lon-1
            DLON(i,j,k) = dx*rad2deg
            DLAT(i,j,k) = dy/wtgu(j)*rad2deg
            XMID(i,j,k) = clon(i,j)*rad2deg
            YMID(i,j,k) = ythu(j)*rad2deg-90.0d0
            YMID_R(i,j,k) = ythu(j)
            AREA_M2(i,j,k) = dA_full(i+1,j)
        end do
        end do
        end do
        do k = 1, num_lev
            do j = idx_start_lat, idx_end_lat
                i = idx_start_lon-1
                XEDGE(i,j,k) = XMID(i,j,k)-DLON(i,j,k)*0.5d0
                do i = idx_start_lon, idx_end_lon-1
                    XEDGE(i,j,k) = (XMID(i-1,j,k)+XMID(i,j,k))*0.5d0
                end do
                i = idx_end_lon
                XEDGE(i,j,k) = XMID(i-1,j,k)+DLON(i-1,j,k)*0.5d0
            end do
            j = idx_start_lat
            YEDGE(:,j,k) = ythv(j)*rad2deg-DLAT(idx_start_lon-1,j,k)-90.0d0
            do j = idx_start_lat+1, idx_end_lat
            do i = idx_start_lon-1, idx_end_lon-1
                YEDGE(i,j,k) = ythv(j-1)*rad2deg-90.0d0
            end do
            end do
            j = idx_end_lat+1
            YEDGE(:,j,k) = ythv(j-2)*rad2deg+DLAT(idx_start_lon-1,j-1,k)-90.0d0
            YEDGE_R(:,:,k) = YEDGE(:,:,k)/rad2deg
        end do
        ! ############################################################################

        call GIGC_INIT_ALL(is_rootproc, input_opt, state_chm, state_met, NC)

        call FILL_CHEM_STATE_NAME_IDS(is_rootproc, state_chm)

        ! TODO: We should set "PEDGE" in GEOS-Chem through "state_met%PEDGE".
        call INIT_PRESSURE(is_rootproc)

!        if (.not. LVARTROP) then
!            call READ_TROPOPAUSE()
!        end if
!
!        if (LEMIS .or. LCHEM) then
!            if (input_opt%ITS_A_FULLCHEM_SIM .or. &
!                input_opt%ITS_AN_AEROSOL_SIM) then
!                call INIT_COMODE(is_rootproc, input_opt, NC)
!            end if
!            if (LKPP) then
!                call INIT_GCKPP_COMODE(is_rootproc, num_lon_subdom, num_lat_subdom, &
!                                       num_lev, ITLOOP, NMTRATE, IGAS, NC)
!            end if
!        end if
!
!        if (LLINOZ) then
!            call LINOZ_READ(is_rootproc, input_opt, NC)
!        end if
!
!        if (ITS_A_CH4_SIM()) then
!            call INIT_GLOBAL_CH4()
!        end if
!
!        if (LMEGAN) then
!            call INIT_MEGAN(state_met)
!        end if
!
        NHMS  = GET_NHMS()
        NHMSb = GET_NHMSb()
        NYMD  = GET_NYMD()
        NYMDb = GET_NYMDb()
!        TAU   = GET_TAU()
!        TAUb  = GET_TAUb()
!
!        ! TODO: Here we should replace the following met reading by transferring
!        !       GAMIL variables.
!        call READ_INITIAL_MET_FIELDS()

        call READ_RESTART_FILE(NYMDb, NHMSb, input_opt, state_met, state_chm)

    end subroutine chem_driver_init

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Transport chemical tracers by using "advection_scheme" module in GAMIL.
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn>
    !
    !       * 2013-03-31 - first create
    !       * 2013-04-15 - split the run into dynamical, physical and
    !                      chemical parts
    !       * 2013-04-19 - add advection codes
    ! --------------------------------------------------------------------------
    
    subroutine chem_driver_run_dynamics(u1, v1, w1, ps1, u2, v2, w2, ps2)

        ! ##############
        ! OLD GAMIL PART
        real(r8), intent(in) :: u1(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        real(r8), intent(in) :: v1(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        real(r8), intent(in) :: w1(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        real(r8), intent(in) :: ps1(ilbnd:ihbnd,beglatexdyn:endlatexdyn)
        real(r8), intent(in) :: u2(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        real(r8), intent(in) :: v2(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        real(r8), intent(in) :: w2(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        real(r8), intent(in) :: ps2(ilbnd:ihbnd,beglatexdyn:endlatexdyn)

        real(r8) q(ilbnd:ihbnd,beglatexdyn:endlatexdyn,num_lev)
        ! ##############

        integer i, j, k, m

        character(*), parameter :: sub_name = "chem_driver_run_dynamics"

        if (.not. is_chem_on) return

        do m = 1, input_opt%N_TRACERS
            ! convert the tracer units from mass within cell to mixing ratio
            do k = 1, num_lev
            do j = idx_start_lat, idx_end_lat
            do i = idx_start_lon, idx_end_lon
                q(i,j,k) = state_chm%TRACERS(i-1,j,k,m)/dA_full(i,j)/ps1(i,j)
            end do
            end do
            end do
            ! ##############
            ! OLD GAMIL PART
            call gamil_arrays_comm(COMM_TO_LEFT,  1, q(:,beglatexdyn,1))
            call gamil_arrays_comm(COMM_TO_RIGHT, 1, q(:,beglatexdyn,1))
            call gamil_arrays_comm(COMM_TO_TOP,   1, q(:,beglatexdyn,1))
            call gamil_arrays_comm(COMM_TO_BOT,   1, q(:,beglatexdyn,1))
            ! ##############
            call advection_scheme_run(u1, v1, w1, ps1, u2, v2, w2, ps2, q)
            do k = 1, num_lev
            do j = idx_start_lat, idx_end_lat
            do i = idx_start_lon, idx_end_lon
                state_chm%TRACERS(i-1,j,k,m) = q(i,j,k)*dA_full(i,j)*ps1(i,j)
            end do
            end do
            end do
        end do

    end subroutine chem_driver_run_dynamics

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Set the meteorological states needed by GEOS-Chem. These states are from
    !   the physics package of GAMIL.
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn>
    !
    !       * 2013-04-15 - first create
    ! --------------------------------------------------------------------------

    subroutine chem_driver_run_physics() ! a long list of input arguments

        character(*), parameter :: sub_name = "chem_driver_run_physics"

        if (.not. is_chem_on) return

    end subroutine chem_driver_run_physics

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Call GEOS-Chem to do chemistry works.
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn>
    !
    !       * 2013-04-15 - first create
    ! --------------------------------------------------------------------------

    subroutine chem_driver_run_chemistry()

        character(*), parameter :: sub_name = "chem_driver_run_chemistry"

        if (.not. is_chem_on) return

    end subroutine chem_driver_run_chemistry

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Finalize the chemistry codes.
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn> - 2013-03-31
    ! --------------------------------------------------------------------------
    
    subroutine chem_driver_final()
    
        character(*), parameter :: sub_name = "chem_driver_final"
    
        if (.not. is_chem_on) return

    end subroutine chem_driver_final

end module chem_driver
