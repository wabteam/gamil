# ------------------------------------------------------------------------------
# Description:
#
#   This is the Makefile for GAMIL.
#
# History:
#
#   2013-05-25:
#
#       [Li Dong]: Rewrite from old Makefile with new features:
#           - The actual Fortran compiler will be judged, so we can set proper
#             options (different compilers have different options).
#           - Let netCDF library set its linking options (-lnetcdff problem).
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# set up special characters
NULL :=
SPACE := $(NULL) $(NULL)
COMMA := $(NULL),$(NULL)

# ------------------------------------------------------------------------------
# set up compilers
CC := cc
ifneq ($(USER_CC),$(NULL))
    CC := $(USER_CC)
endif
FC := mpiifort
ifneq ($(USER_FC),$(NULL))
    FC := $(USER_FC)
endif

# ------------------------------------------------------------------------------
# judge which Fortran compiler do we use
ifneq (,$(shell $(FC) -show | grep -i gfortran))
    FC_VENDOR := GNU
endif
ifneq (,$(shell $(FC) -show | grep -i ifort))
    FC_VENDOR := Intel
endif
ifneq (,$(shell $(FC) -show | grep -i pgf90))
    FC_VENDOR := PGI
endif

# ------------------------------------------------------------------------------
# set up compilation environment
PATHS := . $(shell cat Filepath) $(INC_NETCDF) $(INC_MPI)
SOURCES := $(shell cat Srcfiles)
OBJECTS := $(addsuffix .o, $(basename $(SOURCES)))
VPATH := $(subst $(SPACE),:,$(foreach path,$(PATHS),$(wildcard $(path))))
# - ESMF compilation environment
ESMF_ARCH := linux
ifeq ($(FC_VENDOR),PGI)
    ifeq ($(CC),pgcc)
        ESMF_ARCH = linux_pgi
    else
        ESMF_ARCH = linux_gnupgf90
    endif
endif
ESMF_BOPT := O
ifeq ($(DEBUG),TRUE)
    ESMF_BOPT := g
endif
ESMF_MOD := $(ESMF_BLD)/mod/mod$(ESMF_BOPT)
ESMF_LIB := $(ESMF_BLD)/lib/lib$(ESMF_BOPT)
# - includes and libraries
INCLUDES := $(foreach path,$(PATHS),-I$(path)) -I$(ESMF_MOD)/$(ESMF_ARCH)
LIBRARIES := $(shell nc-config --flibs) -L$(ESMF_LIB)/$(ESMF_ARCH) -lesmf

# ------------------------------------------------------------------------------
# set up compiler options
# - define macros
MACROS = -DLINUX -DUSE_GCC -DCAM -DNO_SHR_VMATH -DHIDE_SHR_MSG
# - C compiler options
CFLAGS += $(INCLUDES) $(MACROS)
ifeq ($(CC),pgcc)
    CFLAGS += -fast
endif
# - Fortran compiler options
ifeq ($(FC_VENDOR),Intel)
    FFLAGS := $(INCLUDES) $(MACROS) -132 -r8 -i4 -g -mp1 -zero -no-vec -O2 \
              -fp-model precise -fp-speculation=off -mt_mpi
    ifeq ($(SMP),TRUE)
        FFLAGS += -openmp -openmp-report1
        LDFLAGS += -openmp
    endif
    ifeq ($(DEBUG),TRUE)
        FFLAGS += -g -check bounds -traceback
    endif
endif

# ------------------------------------------------------------------------------
# set up build targes
all: $(MODEL_EXEDIR)/$(EXENAME)

Depends: Filepath Srcfiles
	$(ROOTDIR)/models/atm/gamil/bld/mkDepends $^ > $@

Srcfiles: Filepath
	$(ROOTDIR)/models/atm/gamil/bld/mkSrcfiles > $@

.SUFFIXES: .F .F90 .c .o

.F90.o:
	$(FC) -c $(FFLAGS) $<
.F.o:
	$(FC) -c $(FFLAGS) $<
.c.o:
	$(CC) -c $(CFLAGS) $<

$(MODEL_EXEDIR)/$(EXENAME): $(OBJECTS)
	$(FC) -o $@ $^ $(LIBRARIES) $(LDFLAGS)

$(ESMF_LIB)/$(ESMF_ARCH)/libesmf.a:
	cd $(ESMF_ROOT); \
    $(MAKE) -j 1 BOPT=$(ESMF_BOPT) ESMF_BUILD=$(ESMF_BLD) ESMF_DIR=$(ESMF_ROOT) ESMF_ARCH=$(ESMF_ARCH);

time_manager.o: $(ESMF_LIB)/$(ESMF_ARCH)/libesmf.a

.PHONY: debug clean
debug:
	@echo -e "Model root directory:\t$(ROOTDIR)"
	@echo -e "Model run directory:\t$(MODEL_EXEDIR)"
	@echo -e "Fortran compiler:\t$(FC_VENDOR) $(FC)"
	@echo -e "ESMF arch:\t\t$(ESMF_ARCH)"
	@echo -e "ESMF options:\t\t$(ESMF_BOPT)"
	@echo -e "Includes:\t\t$(INCLUDES)"
	@echo -e "Libraries:\t\t$(LIBRARIES)"

clean:
	rm -rf esmf
	rm -f Depends Srcfiles *.o *.mod *.stb *.f90 $(MODEL_EXEDIR)/$(EXENAME)

include Depends
